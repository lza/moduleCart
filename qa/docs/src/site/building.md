#要求:
    Linux,本软件是在Deepin系统下开发调试的，windows下可能有些问题，如生成PDF文档时用的Droid Sans Fallback字体在windows下没有。
    java 8。
    [Maven](http://maven.apache.org/) 3.2.1 或更高版本。

#构建方法：
    CD到目录moduleCart/bom， 执行“mvn install”命令  
    在项目根目录执行“mvn install”命令。
#运行：
    如果你只有一个现代浏览器（firefox、chrome、ie10以上），cartSurfacePc工程的target目录中，解压jar文件，复制webjars目录到index.html同级目录，然后打开index.html。
    如果你还有一个tomcat8的web容器，把combined-war.war文件放在tomcat的webapp目录下。
    如果你还有一个数据库，修改persistence.xml文件中的连接信息，并添加数据库jdbc驱动的依赖。
