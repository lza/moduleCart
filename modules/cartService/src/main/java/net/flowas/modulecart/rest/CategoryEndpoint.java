package net.flowas.modulecart.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import net.flowas.modulecart.domain.Category;
@Path("/categories")
public interface CategoryEndpoint extends CrudEndpoint<Category>{
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	Category findById(@PathParam("id") Long id);
	@GET
	@Path("list.json")
	@Produces("application/json")
	List<Category> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult);

}