#moduleCart
模块化的电子商务系统。涵盖b2b,b2c模式的购物交易。</br>
从技术结构上，静态页面和java程序分析，静态页面甚至可以在web容器外运行，即在本地文件夹下直接用浏览器打开。</br>
从独立业务划分上，可以分为身份认证、商品展示、搜索引擎、数据收集与同步等模块。</br>
![模块结构图](moduleCart/raw/master/qa/docs/src/site/images/struc.png "模块结构图")

#构建
[moduleCart/blob/master/qa/docs/src/site/building.md](https://git.oschina.net/flowas/moduleCart/blob/master/qa/docs/src/site/building.md)

#需求管理
需求文档是一个可以导入到[testlink](http://testlink.org/)系统的[xml](https://git.oschina.net/flowas/moduleCart/blob/master/qa/docs/src/site/all-req.xml)文档。

#技术堆栈
java,spring,jpa,jax-rs(cxf),angularjs</br>
测试技术：junit,jetty,htmlunit,jax-rs client</br>
项目管理：maven、docbook、xmlbasedsrs、testlink、git

#为什么选择moduleCart？
创建这个项目是出于技术原因的：</br>
成熟的的商城系统很多，[Comparison of shopping cart software](http://en.wikipedia.org/wiki/Comparison_of_shopping_cart_software) 。</br>
多是PHP的，国内的系统多是基于三大框架，而且分了多层包结构，代码量大，二次开发的工作量很大，没有实现模块化，组件的可重用度低。

#演示程序
可以在 [http://kc.cc/UUle](http://kc.cc/UUle) 找到构建好的文件。</br>
打开连接后，在shared目录下可以看到几个文件，其中demoStatic.zip是纯静态页面，解压后可以直接用浏览器打开。</br>
combined-war-1.0-SNAPSHOT.war文件需要tomcat8来运行，吧包含后台程序。</br>
在京东云擎上也部署一个演示程序，可以直接打开 [http://modulecart.jd-app.com](http://modulecart.jd-app.com) 。

#社区交流
QQ群：345359985
